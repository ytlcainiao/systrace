# systrace

#### 介绍
Python解析systrace获取快速滑动界面的帧率

使用说明：
1、安装Python3以上，将"测试trace.html"和脚本"parse_systrace_tool.py"放到放到同一目录。
   python parse_systrace_tool.py 测试trace.html
2、如果脚本和测试文件不在同一个目录，需要输入"测试trace.html"的完整路径
   python parse_systrace_tool.py x:\xx\xx\测试trace.html


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
